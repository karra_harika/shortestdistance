=======================================================================================
Application--> harika
groupId --> za.co.27four.assignment
artefactId -->harika
version to be 0.0.1_SNAPSHOT
Purpose : to find out the distance between any two planets (source and distance)
========================================================================================

Package Structure
---------------------
za.co.four.assignment.harika --> main class to run the application
za.co.four.assignment.harika.algorithm --> algorithm to find out shortest path
za.co.four.assignment.harika.controller --> Rest Controller for import the file,
					to load the UI and send the request to the service and repository.
za.co.four.assignment.harika.service --> Service classes
za.co.four.assignment.harika.repositoy --> repository classes 
						and CRUD operational methods for graph domain object.
za.co.four.assignment.harika.entity --> entities

graph files
------------------
/resources folder 

Application Port 
--------------------

application start with 8070 port 

Frontend pages
-----------------
html files place under the resources/templates


to start the application 
--------------------------
MainApplication .java ---> starting point of the service 

URLs to the load the UI page 
-----------------------------
http://localhost:8070/ --> it loads the frontend page where you can select source and destination planet

on submission, the URI /findshortestpath will be called and displays the result.

and there is Back button to resubmit the page by selecting different planets.


