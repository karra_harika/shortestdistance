package za.co.four.assignment.harika.repository;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import za.co.four.assignment.harika.entity.PlanetNames;

@Repository
public interface PlanetNameRepository extends CrudRepository<PlanetNames, Long> {
}
