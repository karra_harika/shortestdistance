package za.co.four.assignment.harika.repository;

import org.springframework.data.repository.CrudRepository;

import za.co.four.assignment.harika.entity.Path;


public interface PathRepository extends CrudRepository<Path, Long> {
}
