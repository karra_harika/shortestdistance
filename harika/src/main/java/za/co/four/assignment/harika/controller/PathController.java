package za.co.four.assignment.harika.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import za.co.four.assignment.harika.entity.PlanetNames;
import za.co.four.assignment.harika.entity.Path;
import za.co.four.assignment.harika.repository.*;
import za.co.four.assignment.harika.service.*;


@Controller
public class PathController {
    private static final Logger LOG = LoggerFactory.getLogger(PathController.class);

    @Autowired
    private PathRepository shortestDistanceRepository;

    @Autowired
    private DistanceService shortestPathService;

    
    @GetMapping("/")
    public String loadUIPage(Model model) {
        model.addAttribute("planetNames", new PlanetNames());
        return "display";
    }
    
    

    @PostMapping("/findshortestpath")
    public String findshortestpath(Model model, @ModelAttribute PlanetNames planetNames) {
    	LOG.info("start :: findshortestpath with sourceNode "+planetNames.getPlanetSourceName()+ "and distination Name "+planetNames.getDestinationPlanetName());

        Path shortestDistance = new Path();
        List<Path> listShortestDistance = (List<Path>)shortestDistanceRepository.findAll();
        listShortestDistance.forEach(l -> {
            if (l.getPlanetNode().equalsIgnoreCase(planetNames.getPlanetSourceName())) {
                shortestDistance.setPath(l.getPath());
                
            }
        });
       
        String shortestPath = shortestPathService.shortestPath(planetNames.getPlanetSourceName(), planetNames.getDestinationPlanetName());
        LOG.info("end :: findshortestpath");
        model.addAttribute("shortestPath", shortestPath);
        return "success";
    }
    
   

}

