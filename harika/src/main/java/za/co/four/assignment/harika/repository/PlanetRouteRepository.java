package za.co.four.assignment.harika.repository;

import org.springframework.data.repository.CrudRepository;

import za.co.four.assignment.harika.entity.PlanetRoutes;

public interface PlanetRouteRepository extends CrudRepository<PlanetRoutes, Long>{

}
