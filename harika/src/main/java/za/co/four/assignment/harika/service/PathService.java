package za.co.four.assignment.harika.service;

import java.util.List;

import za.co.four.assignment.harika.repository.PlanetRouteRepository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import za.co.four.assignment.harika.entity.PlanetRoutes;

@Service
public class PathService {

	private static final Logger LOG = LoggerFactory.getLogger(PathService.class);
	
	@Autowired
    PlanetRouteRepository planetRouteRepository;
	
	public List<PlanetRoutes>getAllPlanetRoutes(){
		return (List<PlanetRoutes>) planetRouteRepository.findAll();
	}
	
	public void saveRoute(PlanetRoutes route) {
		LOG.info("start ShortestPathService :: saveRoute");
		planetRouteRepository.save(route);
	}
	
	public void saveorupdateRoute(PlanetRoutes route) {
		LOG.info(" start ShortestPathService :: saveorupdateRoute");
	    PlanetRoutes routeDB= planetRouteRepository.findById(route.getId()).orElse(null);;
        routeDB.setDistance(route.getDistance());
        routeDB.setPlanetDestination(route.getPlanetSource());
        routeDB.setPlanetDestination(route.getPlanetDestination());
		planetRouteRepository.save(routeDB);
	}
	
	
	public void deleteRoute(Long routeId) {
		LOG.info("start ShortestPathService :: deleteRoute");
		planetRouteRepository.deleteById(routeId);
	}
	
}

